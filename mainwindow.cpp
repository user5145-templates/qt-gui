#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionExit_triggered()
{
    exit(0);
}

void MainWindow::on_pushButton_clicked()
{
    if(squreWidthText == 0
            || squreHeightText == 0){
        ui->squareResult->setText("Wrong \nnumber \nformat");
    } else {
        ui->squareResult->setNum(squreWidthText * squreHeightText);
    }
}

void MainWindow::on_inputWidthSquare_textChanged()
{
    QString text = ui->inputWidthSquare->toPlainText();
    squreWidthText = text.toDouble();
}

void MainWindow::on_inputHeightSquare_textChanged()
{
    QString text = ui->inputHeightSquare->toPlainText();
    squreHeightText = text.toDouble();
}

void MainWindow::on_inputWidhtTriangle_textChanged()
{
    QString text = ui->inputWidhtTriangle->toPlainText();
    triangleWidthText = text.toDouble();
}

void MainWindow::on_inputHeightTriangle_textChanged()
{
    QString text = ui->inputHeightTriangle->toPlainText();
    triangleHeightText = text.toDouble();
}

void MainWindow::on_pushButton_2_clicked()
{
    if(triangleWidthText == 0
            || triangleHeightText == 0){
        ui->triangleResult->setText("Wrong \nnumber \nformat");
    } else {
        ui->triangleResult->setNum(triangleWidthText * triangleHeightText /2);
    }
}
