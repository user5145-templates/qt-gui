#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionExit_triggered();

    void on_pushButton_clicked();

    void on_inputWidthSquare_textChanged();

    void on_inputHeightSquare_textChanged();

    void on_inputWidhtTriangle_textChanged();

    void on_inputHeightTriangle_textChanged();

    void on_pushButton_2_clicked();

private:
    double squreWidthText = 0;
    double squreHeightText = 0;
    double triangleWidthText = 0;
    double triangleHeightText = 0;
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
